package controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.io.File;

import com.opencsv.CSVReader;


import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Lista;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {
 
	private MovingViolationsManagerView view;
	
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;
	
	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() {
		view = new MovingViolationsManagerView();
		
		//TODO, inicializar la pila y la cola
		
		movingViolationsQueue = new Lista<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>(0);
		
	}
	
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					this.loadMovingViolations();
					break;
					
				case 2:
					IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
					view.printDailyStatistics(dailyStatistics);
					break;
					
				case 3:
					view.printMensage("Ingrese el número de infracciones a buscar");
					int n = sc.nextInt();

					IStack<VOMovingViolations> violations = this.nLastAccidents(n);
					view.printMovingViolations(violations);
					break;
											
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	

	public void loadMovingViolations() 
	{
		// TODO
		try
		{
      CSVReader reader = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_January_2018_ordered.csv"));
      String[] nextLine;
      reader.readNext();
      while((nextLine = reader.readNext()) != null)
      {
    	
    	VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[12], Integer.parseInt(nextLine[8]), nextLine[11], nextLine[14] );
    	movingViolationsQueue.enqueue(a);
    	movingViolationsStack.push(a);
    	
      }
      CSVReader reader2 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_February_2018_ordered.csv"));
      String[] nextLine2;
      reader2.readNext();
      while((nextLine2 = reader2.readNext()) != null)
      {
    	
    	VOMovingViolations b = new VOMovingViolations(Integer.parseInt(nextLine2[0]), nextLine2[1], nextLine2[12], Integer.parseInt(nextLine2[8]), nextLine2[11], nextLine2[14] );
    	movingViolationsQueue.enqueue(b);
    	movingViolationsStack.push(b);
      }
      
      reader.close();
      reader2.close();
		}
    catch(Exception e)
    {
    	e.getMessage();
    }
	}
	
	public IQueue <VODaylyStatistic> getDailyStatistics ()
	{
		// TODO
		IQueue<VODaylyStatistic> movingViolationsQueue2 = null;
		int q = 0;
		for(int i = 0; i<movingViolationsQueue.size(); i++)
		{
			VOMovingViolations x = (VOMovingViolations) movingViolationsQueue; 
			int pValor = x.getTotalPaid();
			for(int j = 0; j<movingViolationsQueue.size(); j++)
			{
				
				VOMovingViolations y = (VOMovingViolations) movingViolationsQueue; 
				int pValo = y.getTotalPaid();
				if(x.getTicketIssueDate().equals(y.getTicketIssueDate()))
				{
					int wq = 0;
					if(x.getAccidentIndicator().equals(y.getAccidentIndicator()))
							{
						wq ++;
							}
					q ++;
					pValor += pValo;
					VODaylyStatistic z = new VODaylyStatistic(x.getTicketIssueDate(), pValor, q, wq);
					movingViolationsQueue2.enqueue(z);
				}
			}
		}
		return movingViolationsQueue2;
	}
	
	public IStack <VOMovingViolations> nLastAccidents(int n) 
	{
		// TODO
		IStack<VOMovingViolations> retorno = null;
		for(int i = 0; i < movingViolationsStack.size(); i++){
			VOMovingViolations a = (VOMovingViolations) movingViolationsStack;
			if (i <= n){
				int codigo = a.objectId();
				String date = a.getTicketIssueDate();
				String location = a.getLocation();
				String violation = a.getViolationDescription();
				System.out.println(codigo + " " + date + " " + location + " " + violation);
			}
			movingViolationsStack.pop();
			retorno = movingViolationsStack;
		}
		return retorno;
	}
	
}
