package model.vo;

public class VODaylyStatistic
{

	private String Date;
	private int valor;
	private int accidente;
	private int violation;

	public VODaylyStatistic(String pDate, int pValor, int pAccidente, int pViolation)
	{

		Date = pDate;
		valor = pValor;
		accidente =  pAccidente;
		violation = pViolation;
	}
	
	public String getTicketIssueDate() {
		 // TODO Auto-generated method stub
		 return Date;
		}
	public int getTotalPaid() {
		 // TODO Auto-generated method stub
		 return valor;

		}
	public int getAccidentIndicator() {
		 // TODO Auto-generated method stub
		 return accidente;
		}
		 
		/**
		 * @return description - Descripción textual de la infracción.
		 */
		public int  getViolationDescription() {
		 // TODO Auto-generated method stub
		 return violation;
		}

}
