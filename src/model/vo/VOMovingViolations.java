package model.vo;
public class VOMovingViolations {

private int ObjectId;

private String Location;

private String Date;

private int valor;

private String accidente;

private String violation;
     
public VOMovingViolations(int pObjectId, String pLocation, String pDate, int pValor, String pAccidente, String pViolation){
  ObjectId = pObjectId;
  Location = pLocation;
  Date = pDate;
  valor = pValor;
  accidente =  pAccidente;
  violation = pViolation;
}

/**
 * @return id - Identificador único de la infracción
 */
public int objectId() {
 // TODO Auto-generated method stub
 return ObjectId;
} 


/**
 * @return location - Dirección en formato de texto.
 */
public String getLocation() {
 // TODO Auto-generated method stub
 return Location;
}

/**
 * @return date - Fecha cuando se puso la infracción .
 */
public String getTicketIssueDate() {
 // TODO Auto-generated method stub
 return Date;
}

/**
 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
 */
public int getTotalPaid() {
 // TODO Auto-generated method stub
 return valor;

}

/**
 * @return accidentIndicator - Si hubo un accidente o no.
 */
public String  getAccidentIndicator() {
 // TODO Auto-generated method stub
 return accidente;
}
 
/**
 * @return description - Descripción textual de la infracción.
 */
public String  getViolationDescription() {
 // TODO Auto-generated method stub
 return violation;
}
}
